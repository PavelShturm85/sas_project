FROM nginx
ADD conf/nginx/sas.conf /etc/nginx/conf.d/default.conf
CMD ["nginx", "-g", "daemon off;"]
