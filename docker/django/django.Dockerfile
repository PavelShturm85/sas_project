FROM python:3.6.8

WORKDIR /sas

ADD /conf/local_settings/local_settings.py /usr/local/lib/python3.6/local_settings.py


RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -

RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'

RUN apt-get update && apt-get upgrade -y && apt-get install -y  \
    --no-install-recommends apt-utils \
    build-essential libssl-dev libffi-dev python3-dev google-chrome-stable supervisor

ADD /conf/asgi/sas.conf /etc/supervisor/conf.d/sas.conf

ADD /conf/supervisor/supervisord.conf /etc/supervisor/supervisord.conf

RUN mkdir /project_tmp

ADD /sas/requirements.txt /project_tmp/requirements.txt

RUN pip install -r /project_tmp/requirements.txt

ENV NAME sas





