#!/usr/bin/python

import os
commands_for_install = (
    "sudo docker-compose -f docker/docker-compose.yml down --rmi all",
    "git pull",
    "sudo docker-compose -f docker/docker-compose.yml up -d",
    "sudo docker-compose -f docker/docker-compose.yml run django python manage.py migrate",
)

for command in commands_for_install:
    try:
        os.system(command)
    except:
        pass
