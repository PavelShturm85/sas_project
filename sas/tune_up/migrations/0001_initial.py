# Generated by Django 2.2.1 on 2019-06-17 10:53

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='NegativeWords',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('words', models.TextField(verbose_name='Негативные слова')),
            ],
            options={
                'verbose_name': 'Негативные слова',
                'verbose_name_plural': 'Негативные слова',
                'ordering': ['words'],
            },
        ),
        migrations.CreateModel(
            name='PositiveWords',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('words', models.TextField(verbose_name='Позитивные слова')),
            ],
            options={
                'verbose_name': 'Позитивные слова',
                'verbose_name_plural': 'Позитивные слова',
                'ordering': ['words'],
            },
        ),
    ]
