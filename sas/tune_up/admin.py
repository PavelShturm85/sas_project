from django.contrib import admin
from .models import NegativeWords, PositiveWords


# Register your models here.

class NegativeWordsAdmin(admin.ModelAdmin):
    list_display = ['words']


class PositiveWordsAdmin(admin.ModelAdmin):
    list_display = ['words']


admin.site.register(NegativeWords, NegativeWordsAdmin)
admin.site.register(PositiveWords, PositiveWordsAdmin)
