from django.apps import AppConfig


class TuneUpConfig(AppConfig):
    name = 'tune_up'
