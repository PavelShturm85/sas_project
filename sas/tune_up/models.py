import uuid
from django.db import models


# Create your models here.

class NegativeWords(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    words = models.TextField(verbose_name="Негативные слова")

    class Meta:
        ordering = ['words']
        verbose_name = 'Негативные слова'
        verbose_name_plural = 'Негативные слова'



class PositiveWords(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    words = models.TextField(verbose_name="Позитивные слова")

    class Meta:
        ordering = ['words']
        verbose_name = 'Позитивные слова'
        verbose_name_plural = 'Позитивные слова'

