from django.shortcuts import render, get_object_or_404, render_to_response
from django.views.generic.base import TemplateView
from django.urls import reverse_lazy
from .models import Company
from news.models import News
from .filter import CompaniesFilter
from django.core import serializers
# Create your views here.

class CompaniesList(TemplateView):
    template_name = 'company/companies_list.html'

    # def get_context_data(self, *args, **kwargs):
        # context = super().get_context_data(**kwargs)
        # companies_list = Company.objects.all()
        # context['filter'] = CompaniesFilter(self.request.GET, queryset=companies_list)
        # return {}


class CompanyDetail(TemplateView):
    template_name = 'company/company_detail.html'
    success_url = reverse_lazy('companies_list')

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     context['company'] = get_object_or_404(Company, pk=self.kwargs['pk'])
    #     context['news_list'] = News.objects.filter(company=self.kwargs['pk']).order_by('-date')
    #     return context
