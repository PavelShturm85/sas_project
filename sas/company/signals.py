import json
from django.db.models.signals import post_save
from django.dispatch import receiver
from company.models import Company
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
from .company_settings import COMPANIES_LIST_ROOM_NAME


@receiver(post_save, sender=Company, dispatch_uid="added_new_company")
def added_new_company_receive(sender, instance, **kwargs):
    name = str(instance.short_name)
    inn = str(instance.inn)
    _id = str(instance.id)
    data = dict(newCompany={
                'name': name,
                'inn': inn,
                'id': _id,
                }
            )
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        COMPANIES_LIST_ROOM_NAME, {
            'type': 'send_to_page',
            'text': json.dumps(data),
        }
    )

