import uuid
from django.db import models
from django.urls import reverse


# Create your models here.

class Company(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    full_name = models.CharField(verbose_name='Полное название', max_length=200, blank=True, default="")
    short_name = models.CharField(verbose_name='Короткое название', max_length=200, blank=True, default="")
    inn = models.CharField(verbose_name="ИНН", max_length=12, unique=True)
    web_site = models.URLField(verbose_name='Сайт компании', max_length=250, blank=True, default="")
    availability_web_site = models.BooleanField(verbose_name="Доступность сайта", blank=True, default=False)
    date_update_web_site = models.CharField(verbose_name="Дата обновления сайта", max_length=12, blank=True, default="")

    class Meta:
        ordering = ['short_name']
        verbose_name = 'Компания'
        verbose_name_plural = 'Компании'

    def __str__(self):
        return '{} {} {}'.format(self.short_name, self.availability_web_site, self.date_update_web_site)

    def __iter__(self):
        for field in self._meta.fields:
            yield (field.verbose_name, field.value_to_string(self))


class ExecutiveProduction(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    company = models.ForeignKey(Company,
                                verbose_name="Компания", on_delete=models.CASCADE)
    number = models.CharField(verbose_name="Номер", max_length=50, unique=True)
    date = models.DateField(verbose_name="Дата")
    STATUS_CHOICES = (
        ('open', 'Открыто'),
        ('close', 'Завершено'),
    )
    status = models.CharField(max_length=9, choices=STATUS_CHOICES, verbose_name='Статус')
    closing_date = models.DateField(verbose_name="Дата завершения", blank=True)
    balance = models.DecimalField(verbose_name="Сумма", decimal_places=2, max_digits=31)
    outstanding_balance = models.DecimalField(verbose_name="Непогашенный остаток", decimal_places=2, max_digits=31,
                                              default=balance)
    subject_execution = models.CharField(verbose_name="Предмет исполнения", max_length=500, default="")
    requisites_executive_doc = models.CharField(verbose_name="Реквизиты исполнительного документа", max_length=500,
                                                default="")

    class Meta:
        ordering = ['number']
        verbose_name = 'Исполнительное производство'
        verbose_name_plural = 'Исполнительные производства'

    def __str__(self):
        return '{} {} {} {}'.format(self.company, self.date, self.number, self.status)


class CourtCase(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    company = models.ForeignKey(Company,
                                verbose_name="Компания", on_delete=models.CASCADE)
    number = models.CharField(verbose_name="Номер", max_length=50, unique=True)
    date = models.DateField(verbose_name="Дата")
    type = models.CharField(max_length=150, verbose_name='Тип', default="")
    balance = models.DecimalField(verbose_name="Сумма", decimal_places=2, max_digits=31)
    STATUS_CHOICES = (
        ('open', 'Рассматривается'),
        ('close', 'Завершено'),
    )
    status = models.CharField(max_length=9, choices=STATUS_CHOICES, verbose_name='Статус')
    instance = models.CharField(max_length=150, verbose_name='Инстанция', default=date)
    updated_date = models.DateField(verbose_name="Дата обновления", default="")
    plaintiff = models.TextField(verbose_name='Истец', default="")
    defendant = models.TextField(verbose_name='Ответчик', default="")
    other_person = models.TextField(verbose_name='Иные лица', default="")
    third_parties = models.TextField(verbose_name='Третьи лица', default="")
    last_document = models.CharField(max_length=150, verbose_name='Последний документ', default="")

    class Meta:
        ordering = ['number']
        verbose_name = 'Судебное дело'
        verbose_name_plural = 'судебные дела'

    def __str__(self):
        return '{} {} {} {}'.format(self.company, self.date, self.number, self.status)
