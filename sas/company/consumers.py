import asyncio
from channels.consumer import AsyncConsumer
import json
from django.forms.models import model_to_dict
from channels.db import database_sync_to_async
from .company_settings import COMPANIES_LIST_ROOM_NAME, COMPANY_DETAIL_ROOM_NAME
from company.models import Company
from news.models import News
from company.tasks import add_new_company
from channels.exceptions import StopConsumer


class CompaniesListConsumer(AsyncConsumer):
    async def websocket_connect(self, event):
        print("connection CompaniesListConsumer successfull:", event)
        await self.send({
            'type': 'websocket.accept'
        })

        self.room_name = COMPANIES_LIST_ROOM_NAME
        # Join room group
        await self.channel_layer.group_add(
            self.room_name,
            self.channel_name,
        )
        data = await self.companies_list()
        await self.channel_layer.group_send(
            self.room_name, {
                'type': 'send_to_page',
                'text': json.dumps(data),
            }
        )


    async def websocket_disconnect(self, event):
        print("disconnect CompaniesListConsumer successfull:", event)
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_name,
            self.channel_name
        )
        raise StopConsumer()


    async def websocket_receive(self, event, **kwargs):
        data_json = event.get('text')
        data = json.loads(data_json)
        inn = data.get("inn")
        if inn:
            add_new_company.delay(inn)

    async def send_to_page(self, event):
        await self.send({
            'type': 'websocket.send',
            'text': event['text']
        }
        )

    @database_sync_to_async
    def companies_list(self):
        data = [dict(id=str(company.id), name=company.short_name, inn=company.inn) for company in Company.objects.all()]
        return dict(companiesList=data)


class CompanyDetailConsumer(AsyncConsumer):
    async def websocket_connect(self, event):
        print("connection CompanyDetailConsumer successfull:", event)
        await self.send({
            'type': 'websocket.accept'
        })

        self.id_room = self.scope['url_route']['kwargs']['pk']

        self.room_name = f"{COMPANY_DETAIL_ROOM_NAME}_{self.id_room}"
        # Join room group
        await self.channel_layer.group_add(
            self.room_name,
            self.channel_name,
        )

        news = await self.news_list()
        company = await self.company_detail()
        for data in (news, company):
            await self.channel_layer.group_send(
                self.room_name, {
                    'type': 'send_to_page',
                    'text': json.dumps(data),
                }
            )


    async def websocket_disconnect(self, event):
        print("disconnect CompanyDetailConsumer successfull:", event)
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_name,
            self.channel_name
        )
        raise StopConsumer()


    async def websocket_receive(self, event, **kwargs):
        pass

    async def send_to_page(self, event):
        await self.send({
            'type': 'websocket.send',
            'text': event['text']
        }
        )

    @database_sync_to_async
    def news_list(self):
        data = [dict(id=str(news.id),
                     status=news.status,
                     title=news.title,
                     body=news.body,
                     date=news.date.strftime("%Y-%m-%d %H:%M:%S")) for news in
                News.objects.filter(company=self.id_room).order_by('-date')]
        return dict(newsList=data)

    @database_sync_to_async
    def company_detail(self):
        data = [{"name": name, "val": val} for name, val in Company.objects.get(id=self.id_room) if name != "id"]
        return dict(companyDetail=data)
