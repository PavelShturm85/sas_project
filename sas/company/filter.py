import django_filters
from .models import Company
from django.db import models


class CompaniesFilter(django_filters.FilterSet):

    class Meta:
        model = Company
        fields = ['short_name', 'inn',]

        filter_overrides = {
            models.CharField: {
                'filter_class': django_filters.CharFilter,
                'extra': lambda f: {
                    'lookup_expr': 'icontains',
                },
            },

        }
