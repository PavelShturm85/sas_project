from django.urls import path
from . import views

urlpatterns = [
    path('company/<pk>/', views.CompanyDetail.as_view(), name='company_detail'),
    path('', views.CompaniesList.as_view(), name='companies_list'),
    # path('tune_up/', views.TuneUpList.as_view(), name='tune_up_list'),


]