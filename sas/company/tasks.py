import os
from celery import task
import re
import django
from company.handler.get_company_name import company_name
from company.handler.web_site_availability import site_availability
from company.models import Company
from news.tasks import get_news_by_company

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sas.settings')
django.setup()


@task(name="Добавление новой компании", queue='low', default_retry_delay=300, max_retries=5)
def add_new_company(inn):
    is_company_in_db = Company.objects.filter(inn=inn).exists()
    if not is_company_in_db:
        full_name, short_name = company_name(inn)
        Company.objects.create(
            full_name=full_name,
            short_name=short_name,
            inn=inn,
        )
        get_news_by_company.delay(inn)


@task(name="Создать задачи по проверке УРЛ", queue='medium', default_retry_delay=300, max_retries=5)
def check_url():
    companies = Company.objects.all()
    for company in companies:
        url = company.web_site
        if url:
            http_temp = "http://"
            www_temp = "www."
            url = url.replace(www_temp, "").replace(http_temp, "")
            is_kirilic = bool(re.search(r"[А-Яа-я]", url))

            if is_kirilic:
                url = f"{http_temp}{url}"
            else:
                url = f"{http_temp}{www_temp}{url}"
            check_site.delay(url, company.id)


@task(name="Проверка доступности сайта", queue='low', default_retry_delay=300, max_retries=5)
def check_site(url, id):
    data = site_availability(url)
    _save_web_data.delay(data, id)


@task(name="Сохранить данные сайты", queue='high', default_retry_delay=60)
def _save_web_data(data, id):
    Company.objects.filter(id=id).update(
        availability_web_site=data.get('availability', False),
        date_update_web_site=data.get('date', ""),
    )
