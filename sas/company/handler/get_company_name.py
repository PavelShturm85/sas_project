import os
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sas.settings')
django.setup()


def init_driver():
    options = webdriver.ChromeOptions()
    options.add_argument('--headless')
    options.add_argument('--disable-gpu')
    options.add_argument('--disable-dev-shm-usage')
    options.add_argument('--no-sandbox')
    chrome = webdriver.Chrome(executable_path="./chromedriver", chrome_options=options)
    chrome.implicitly_wait(7)
    chrome.set_window_size(1920, 1080)
    chrome.maximize_window()
    return chrome

def company_name(inn):
    settings = {
        "input_form": "input.index-search-input",
        "chose_title": "div.company-item div.company-item__title a",
        "chose_inn": "div.company-item-info dl dd span.finded-text",
        "menu_more": "span.company-menu_more",
        "requisites": "a.gtm_nav_requisites",
        "full_name": '//*[@id="anketa"]/div[1]/div[1]',
        "short_name": '//*[@id="main"]/div/div[1]/div[1]/h1',
    }

    aggregator = "https://www.rusprofile.ru/"
    driver = init_driver()
    driver.get(aggregator)

    input_form = WebDriverWait(driver, 7).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, settings["input_form"])))
    input_form.send_keys(inn)
    input_form.submit()

    try:
        titles = WebDriverWait(driver, 3).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, settings["chose_title"])))
        inns = WebDriverWait(driver, 3).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, settings["chose_inn"])))
        for c_title, c_inn in zip(titles, inns):

            if c_inn.text == inn:
                c_title.click()
                break
    except:
        pass

    full_name = WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.XPATH, settings["full_name"])))
    short_name = WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.XPATH, settings["short_name"])))
    return (full_name.text, short_name.text)


if __name__ == "__main__":
    inn = "6668020461"
    company_name(inn)
