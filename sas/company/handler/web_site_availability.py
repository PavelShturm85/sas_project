import requests
import htmldate
from lxml import html

def site_availability(url):
    def check_page(url):
        '''Checking news page.'''
        try:
            r = requests.get(url, timeout=10, verify=False)
            if r.ok:
                return r.text
        except:
            pass

    def last_update(html_text):
        '''htmldate library. Searches date into html code.'''
        mytree = html.fromstring(html_text)
        date = htmldate.find_date(mytree, outputformat='%d-%m-%Y')
        return date

    def get_date(url):
        html_text = check_page(url)
        if html_text:
            date = last_update(html_text)
            return date

    availability = check_page(url)
    date = ""
    if availability:
        regex_news = r'news|blog'
        try:
            r = requests.get(url + 'sitemap.xml')
            tree = html.fromstring(r.text.encode('utf-8'))
            list_urls = tree.xpath("//url/loc/text()")
            for _url in list_urls:
                is_url = re.findall(regex_news, _url)
                if is_url:
                    date = get_date(_url)
                    if date:
                        break
        except:
            pass

        if not date:
            date = last_update(availability)

    data = {'url': url, 'availability': bool(availability), 'update': bool(date), 'date': date or ""}
    return data