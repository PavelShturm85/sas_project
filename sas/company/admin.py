from django.contrib import admin
from .models import Company, ExecutiveProduction, CourtCase


# Register your models here.

class CompanyAdmin(admin.ModelAdmin):
    list_filter = ('short_name', 'inn')
    list_display = ['short_name', 'inn', 'availability_web_site', 'date_update_web_site']
    search_fields = ['short_name', 'inn']

class ExecutiveProductionAdmin(admin.ModelAdmin):
    list_display = ['company', 'date', 'number', 'status']

class CourtCaseAdmin(admin.ModelAdmin):
    list_display = ['company', 'date', 'number', 'status']


admin.site.register(Company, CompanyAdmin)
admin.site.register(ExecutiveProduction, ExecutiveProductionAdmin)
admin.site.register(CourtCase, CourtCaseAdmin)
