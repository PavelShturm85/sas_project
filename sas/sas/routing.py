from channels.routing import ProtocolTypeRouter, URLRouter
from channels.security.websocket import AllowedHostsOriginValidator
from channels.auth import AuthMiddlewareStack
from django.urls import path
from company.consumers import CompaniesListConsumer, CompanyDetailConsumer

application = ProtocolTypeRouter({
    'websocket': AllowedHostsOriginValidator(
        AuthMiddlewareStack(
            URLRouter([
                # Копируем из урл из urls.py и консумер обрабатывающий этот урл.
                path('', CompaniesListConsumer),
                path('company/<pk>/', CompanyDetailConsumer),
            ])
        )
    )
})
