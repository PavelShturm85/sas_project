from django.urls import path
from . import views

urlpatterns = [
    path('<pk>/', views.NewsDetail.as_view(), name='news_detail'),
]
