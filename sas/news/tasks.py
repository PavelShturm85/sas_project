import os
from celery import task
from news.handler.news_aggregators import methods
from news.handler.word_processing import scrap, normalize_name_company, normalize_yandex_date, tonality_news, \
    SearchWordInMessage
from company.models import Company
from news.models import News
import django
from django.utils.timezone import make_aware

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sas.settings')
django.setup()


@task(name="Сбор новостей", queue='medium', default_retry_delay=300, max_retries=5)
def collecting_news():
    full_name_list = ({"name": normalize_name_company(company.full_name), "id": company.id, "inn": company.inn} for company in
                      Company.objects.all() if company.full_name)
    short_name_list = ({"name": company.short_name, "id": company.id, "inn": company.inn} for company in Company.objects.all() if
                       company.short_name)
    aggr_list = list(methods.keys())
    data = (full_name_list, short_name_list)
    for name_list in data:
        for company in name_list:
            get_news.delay(company, aggr_list)


@task(name="Сбор новостей по компании", queue='medium', default_retry_delay=300, max_retries=5)
def get_news_by_company(inn):
    company = Company.objects.get(inn=inn)
    short_name = company.short_name
    full_name = normalize_name_company(company.full_name)
    aggr_list = list(methods.keys())
    id = company.id
    for name in (short_name, full_name):
        if name:
            get_news.delay({"id": id, "name": name, "inn": inn}, aggr_list)


@task(name="Создать задачи по сбору новостей", queue='medium', default_retry_delay=300, max_retries=5)
def get_news(company, aggr_list):
    [_get_urls_news_and_start_scrap.delay(company, aggregator) for aggregator in aggr_list]


@task(name="Получить УРЛы новостей", queue='resource_intensive', default_retry_delay=900, max_retries=5)
def _get_urls_news_and_start_scrap(company, aggregator):
    # name_company = normalize_name_company(company["name"])
    name_company = company["name"]
    inn_company = company["inn"]
    data = methods[aggregator](name_company, inn_company)
    for url, date in data:
        _scrap_news_and_save.delay(url, date, company)


@task(name="Получить новость", queue='low', default_retry_delay=300, max_retries=5)
def _scrap_news_and_save(url, date, company):
    data = scrap(url)
    if data:
        text = data["text"]
        is_name_company_in_text = SearchWordInMessage.is_word_in_text(company["name"], text)

        if is_name_company_in_text:
            url = data["url"]
            status = tonality_news(text)
            data = dict(
                title=data["title"],
                body=text,
                date=date,
                url=url,
                status=status
            )
            _save_news.delay(data, company)


@task(name="Сохранить новость", queue='high', default_retry_delay=60)
def _save_news(data, company):
    company_id = company["id"]
    news_url = data["url"]
    company_obj = Company.objects.get(id=company_id)
    date = normalize_yandex_date(data["date"])
    aware_datetime = make_aware(date)
    is_url_in_db = News.objects.filter(company__id=company_id).filter(url=news_url).exists()
    if not is_url_in_db:
        News.objects.create(
            company=company_obj,
            title=data["title"],
            body=data["body"],
            date=aware_datetime,
            url=news_url,
            status=data["status"]
        )

# @task(name="Сохранить в монго", queue='high', default_retry_delay=60)
# def save_news_to_mongo():
#     list_companies = Company.objects.all()
#     from pymongo import MongoClient
#     mongo = MongoClient('127.0.0.1', 27017, connect=False)
#     db = mongo.company
#     company = db.company
#     for _comp in list_companies:
#         data = dict(
#             full_name=_comp.full_name,
#             short_name=_comp.short_name,
#             inn=_comp.inn,
#         )
#         company.save(data)

@task(name="Сбор новостей по идр", queue='medium', default_retry_delay=300, max_retries=5)
def collecting_news_by_id():
    companies_id = [company.id for company in Company.objects.all() if News.objects.filter(company__id=company.id).count() < 5]
    for _id in companies_id:
        company = Company.objects.get(id=_id)
        short_name = company.short_name
        full_name = normalize_name_company(company.full_name)
        aggr_list = list(methods.keys())
        for name in (short_name, full_name):
            if name:
                get_news.delay({"id": _id, "name": name, "inn": company.inn}, aggr_list)


@task(name="Сохранить в монго", queue='high', default_retry_delay=60)
def save_news_to_mongo():
    list_news = News.objects.all()
    from pymongo import MongoClient
    mongo = MongoClient('127.0.0.1', 27017, connect=False)
    db = mongo.news
    news = db.by_company
    for _news in list_news:
        print(_news)
        data = dict(
            name_company=str(_news.company),
            title=_news.title,
            text=_news.body,
            date_publish=_news.date,
            url=_news.url,
            status_re=_news.status,
            status_ui="",
        )
        news.save(data)

@task(name="из орм в цсв", queue='high', default_retry_delay=60)
def save_news_to_csv():
    companies = Company.objects.all()
    # date_list = ["2019", "2018","2017", "2016","2015", "2014","2013", "2012","2011", "2010","2009", "2008", "2007", "2006", "2004", "2003"]
    settings = {
        "FILENAME": "/home/pshturm/projects/test_news_please/company_web_site.csv",
        "columns": ["name", "inn", "web_site","availability_web_site", "date_update_web_site"],
    }
    # settings["columns"].extend(date_list)
    import csv
    with open(settings['FILENAME'], "w", newline="") as file:
        writer = csv.DictWriter(file, fieldnames=settings['columns'])
        writer.writeheader()
        for company in companies:
            data={}
            data["name"] = company.short_name
            data["inn"] = company.inn
            data["web_site"] = company.web_site
            data["availability_web_site"] = company.availability_web_site
            data["date_update_web_site"] = company.date_update_web_site

            # запись одной строки
            writer.writerow(data)

