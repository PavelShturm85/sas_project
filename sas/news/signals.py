import json
from django.db.models.signals import post_save
from django.dispatch import receiver
from news.models import News
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
from company.company_settings import COMPANY_DETAIL_ROOM_NAME


@receiver(post_save, sender=News, dispatch_uid="added_new_news")
def added_new_news_receive(sender, instance, **kwargs):
    title = str(instance.title)
    body = str(instance.body)
    status = str(instance.status)
    date = instance.date.strftime("%Y-%m-%d %H:%M:%S")
    _id = str(instance.id)
    company_id = str(instance.company.id)
    data = dict(newNews=dict(id=_id,
                             title=title,
                             body=body,
                             date=date,
                             status=status,
                        )
            )
    current_room = f"{COMPANY_DETAIL_ROOM_NAME}_{company_id}"
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        current_room, {
            'type': 'send_to_page',
            'text': json.dumps(data),
        }
    )
