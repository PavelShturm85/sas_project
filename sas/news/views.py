from django.shortcuts import render, get_object_or_404
from django.views.generic.base import TemplateView
from django.urls import reverse_lazy
from .models import News


# Create your views here.

class NewsDetail(TemplateView):
    template_name = 'news/news_detail.html'
    success_url = reverse_lazy('company_detail')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['news'] = get_object_or_404(News, pk=self.kwargs['pk'])
        return context
