import os
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from news.models import News

import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sas.settings')
django.setup()


def init_driver():
    options = webdriver.ChromeOptions()
    options.add_argument('--headless')
    options.add_argument('--disable-gpu')
    options.add_argument('--disable-dev-shm-usage')
    options.add_argument('--no-sandbox')
    chrome = webdriver.Chrome(executable_path="./chromedriver", chrome_options=options)
    chrome.implicitly_wait(7)
    chrome.set_window_size(1920, 1080)
    chrome.maximize_window()
    return chrome


def get_ya_news_urls(name_company, inn_company):
    settings = {
        "input_form": "input.input__control",
        "news_button": "div.service_name_news",
        "date_sort": "span.radio-button label.radio-button__radio_side_left",
        "urls": "li.search-item a.link.link_theme_normal.i-bem",
        "dates": "div.document__time",
        "button_next": "a.button",
    }

    aggregator = "https://yandex.ru/"
    driver = init_driver()
    try:
        driver.get(aggregator)

        input_form = WebDriverWait(driver, 7).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, settings["input_form"])))
        input_form.send_keys(name_company)
        input_form.submit()

        try:
            WebDriverWait(driver, 9).until(EC.alert_is_present())
            alert = driver.switch_to.alert
            alert.accept()
        except:
            pass

        news_button = WebDriverWait(driver, 9).until(EC.element_to_be_clickable((By.CSS_SELECTOR, settings["news_button"])))
        news_button.click()

        window_after = driver.window_handles[1]
        driver.switch_to.window(window_after)

        date_sort = WebDriverWait(driver, 7).until(EC.element_to_be_clickable((By.CSS_SELECTOR, settings["date_sort"])))
        date_sort.click()

        input_form = WebDriverWait(driver, 7).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, settings["input_form"])))
        input_form.submit()

        date_sort = WebDriverWait(driver, 7).until(EC.presence_of_element_located((By.CSS_SELECTOR, settings["date_sort"])))
        date_sort.click()

        while True:
            urls = WebDriverWait(driver, 7).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, settings["urls"])))
            url_list = [url.get_attribute("href") for url in urls]
            dates = WebDriverWait(driver, 7).until(
                EC.presence_of_all_elements_located((By.CSS_SELECTOR, settings["dates"])))
            date_list = [date.text for date in dates]
            for url, date in zip(url_list, date_list):
                is_url_in_db = News.objects.filter(company__inn=inn_company).filter(url=url).exists()
                if not is_url_in_db:
                    yield (url, date)
            try:
                button_next = WebDriverWait(driver, 7).until(
                    EC.presence_of_all_elements_located((By.CSS_SELECTOR, settings["button_next"])))[-1]
            except:
                break
            if "Следующая" not in button_next.text:
                break
            button_next.click()
    except:
        pass
    finally:
        driver.quit()


methods = {
    "yandex": get_ya_news_urls,
    # "google": get_google_news_urls,
}

if __name__ == "__main__":
    name_company = "ПО монтажник"
    # 7446006468 7728869293
    get_ya_news_urls(name_company)
