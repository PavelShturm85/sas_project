import os
import regex
import re
import django
from tune_up.models import PositiveWords, NegativeWords
from newsplease import NewsPlease
from dateutil import parser
from datetime import timedelta

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sas.settings')
django.setup()


def scrap(url):
    url = url.replace("\n", " ").strip()
    try:
        article = NewsPlease.from_url(url)
    except:
        return

    text = article.text

    if text:
        title = article.title
        if title:
            title = title.replace("\n", " ")
        else:
            title = text[:15] + "..."
        is_kirilic = bool(re.search(r"[А-Яа-я]", text))
        if is_kirilic:
            data = dict(
                title=title,
                text=text.replace("\n", " "),
                url=url
            )
            return data


def normalize_name_company(name_company):
    name = " ".join(name_company.split('"')[1:]).strip()
    return f'"{name}"'


def normalize_yandex_date(date):
    repl_dict = dict(
        января="January",
        февраля="February",
        марта="March",
        апреля="April",
        мая="May",
        июня="June",
        июля="July",
        августа="August",
        сентября="September",
        октября="October",
        ноября="November",
        декабря="December",
    )
    date = date.replace(" в", "")

    if "вчера" in date:
        date = date.split()[1]
        dt = parser.parse(date)
        normalize_date = dt - timedelta(days=1)
    else:
        for key in repl_dict.keys():
            if key in date:
                date = date.replace(key, repl_dict[key]).replace(" .", ".")
                break
        normalize_date = parser.parse(date)

    return normalize_date


class SearchWordInMessage():

    @staticmethod
    def is_word_in_text(word, text):
        word = word.lower()
        text = text.lower()
        len_word = len(word)
        if 8 >= len_word >= 6:
            accuracy = 1
        elif 11 >= len_word > 8:
            accuracy = 2
        elif 17 >= len_word > 11:
            accuracy = 3
        elif len_word > 17:
            accuracy = 4
        else:
            accuracy = 0

        accuracy_template = "{e<=%s}" % accuracy
        template_search = f'({word}){accuracy_template}'
        regex_search = regex.search(
            template_search, text, flags=regex.IGNORECASE)
        return bool(regex_search)

    @staticmethod
    def is_list_word_in_text_regex(message, words_list):
        i = 0
        for phrase in words_list:
            is_word_in_text = SearchWordInMessage().is_word_in_text(phrase, message)
            if is_word_in_text:
                i += 1
        return i


def tonality_news(text):
    NEGATIVE_LIST = [t.words for t in NegativeWords.objects.all()][0].split("\n")
    is_negative = SearchWordInMessage().is_list_word_in_text_regex(text, NEGATIVE_LIST)

    POSITIVE_LIST = [t.words for t in PositiveWords.objects.all()][0].split("\n")
    is_positive = SearchWordInMessage().is_list_word_in_text_regex(text, POSITIVE_LIST)
    if is_negative > is_positive:
        return "negative"
    elif is_positive > is_negative:
        status = "positive"
    else:
        status = "neutral"

    return status
