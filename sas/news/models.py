import uuid
from django.db import models
from company.models import Company


# Create your models here.

class News(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    company = models.ForeignKey(Company,
                                verbose_name="Компания", on_delete=models.CASCADE)
    title = models.CharField(verbose_name='Заголовок новости', max_length=500)
    body = models.TextField(verbose_name="Текст новости")
    url = models.CharField(verbose_name='УРЛ новости', max_length=3000)
    date = models.DateTimeField(verbose_name='Дата новости')
    STATUS_CHOICES = (
        ('negative', 'Негативная'),
        ('positive', 'Позитивная'),
        ('neutral', 'Нейтральная'),
    )
    status = models.CharField(max_length=12, choices=STATUS_CHOICES, verbose_name='Статус', default="")

    class Meta:
        ordering = ['title']
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'

    def __str__(self):
        return '{} {} {}'.format(self.title, self.date, self.company)
