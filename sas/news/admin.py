from django.contrib import admin
from .models import News


# Register your models here.

class NewsAdmin(admin.ModelAdmin):
    list_display = ['company', 'title', 'date', 'status']
    list_filter = ['company', 'date', 'status']


admin.site.register(News, NewsAdmin)
