DEBUG = False

ALLOWED_HOSTS = ['*',]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'sas_db',
        'USER': 'sas_admin',
        'PASSWORD': 'Qwert12345%',
        'HOST': 'db',
        'PORT': '5432',
    }
}

CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_rabbitmq.core.RabbitmqChannelLayer",
        "CONFIG": {
            "host": "amqp://guest:guest@rabbitmq:5672//",
            # "ssl_context": ... (optional)
        },
    },
}

CELERY_BROKER_URL = 'amqp://guest:guest@rabbitmq:5672//'
CELERY_RESULT_BACKEND = 'rpc://guest:guest@rabbitmq:5672//'
